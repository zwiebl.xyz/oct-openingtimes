<?php
namespace Zwiebl\Openingtimes\Components;

use Spatie\OpeningHours\OpeningHours;
use Carbon\Carbon;
use DateTime;
use Zwiebl\Openingtimes\Models\Settings;

class ShowText extends \Cms\Classes\ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Show Text',
            'description' => 'Displays text with next opening time'
        ];
    }

    public function onRun() {

    }


}