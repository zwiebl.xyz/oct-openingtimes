<?php namespace Zwiebl\Openingtimes;

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Zwiebl\Openingtimes\Components\ListOpeningTimes' => 'openingTimes'
        ];
    }

    public function registerSettings()
    {
        return [
            'location' => [
                'label'       => 'OpeningTimes',
                'description' => 'Manage Opening times',
                'category'    => 'Settings',
                'icon'        => 'icon-clock-o',
                'class'       => 'Zwiebl\OpeningTimes\Models\Settings',
                'order'       => 500,
                'keywords'    => 'geography place placement'
            ]
        ];
    }
}
