<?php namespace Zwiebl\Openingtimes\Models;

use Model;
use BackendMenu;

class Settings extends Model
{

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Zwiebl.Airsoftpark', 'apt-main', 'apt-side-openingtimes');
    }

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'openingTimes';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}